Ois calendar sync(Lõpu projekt)
=================
This application syncs your timetable from ÕIS to your wished google calendar
## Requirements
* Java 1.8
###### Packages:
* com.google.api-client:google-api-client-android:1.22.0
* com.google.apis:google-api-service-calendar:v3-rev225-1.22.0
* com.google.oauth-client:google-oauth-client-jetty:1.22.0
* org.jsoup:jsoup:1.10.1