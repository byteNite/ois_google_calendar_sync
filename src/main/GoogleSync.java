package main;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.client.util.DateTime;

import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * A simple google calendar api class which is built up on google calendar api quickstart.
 * https://developers.google.com/google-apps/calendar/quickstart/java
 * It is possible to authorize against google, get a list of users calendars and add events
 * Created by erik.schults on 01.12.2016.
 */
class GoogleSync {
    /**
     * Application name
     */
    private static final String APPLICATION_NAME =
            "Ois Calendar Sync";

    private static String calendarId;

    private static final java.io.File DATA_STORE_DIR =
            new java.io.File(System.getProperty("user.home"), ".credential/ois-calendar-sync");

    private static FileDataStoreFactory DATA_STORE_FACTORY;

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static HttpTransport HTTP_TRANSPORT;

    private static final List<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR);

    private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");


    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            t.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Creates an authorized Credential object
     *
     * @return an authorized Credential object
     * @throws IOException
     */
    private static Credential authorize() throws IOException {
        // load client secrets.
        InputStream in =
                GoogleSync.class.getResourceAsStream("client_secret.json");
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow
                        .Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();

        Credential credential =
                new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver())
                        .authorize("user");

        System.out.println(
                "Credentials saved to: " + DATA_STORE_DIR.getAbsolutePath());
        return credential;
    }

    static com.google.api.services.calendar.Calendar
    getCalendarService() throws IOException {
        Credential credential = authorize();
        return new com.google.api.services.calendar.Calendar.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    /**
     * Creates an event in google calendar
     *
     * @throws IOException
     */
    void addEvent(com.google.api.services.calendar.Calendar service, Lecture lecture)
            throws IOException, ParseException {

        StringBuilder desc = new StringBuilder(lecture.type + " ");
        desc.append(lecture.teacher + " ");
        desc.append(lecture.groups);

        Event event = new Event()
                .setSummary(lecture.name)
                .setLocation(lecture.location)
                .setDescription(desc.toString());

        // startdate
        DateTime startTime = new DateTime(df.parse(lecture.getStartDateTime()));
        EventDateTime start = new EventDateTime().setDateTime(startTime);
        event.setStart(start);

        // enddate
        DateTime endTime = new DateTime(df.parse(lecture.getEndDateTime()));
        EventDateTime end = new EventDateTime()
                .setDateTime(endTime);
        event.setEnd(end);

        // minutes before notification
        EventReminder[] reminerOverrides = new EventReminder[]{
                new EventReminder().setMethod("popup").setMinutes(30)};
        Event.Reminders reminders = new Event.Reminders()
                .setUseDefault(false)
                .setOverrides(Arrays.asList(reminerOverrides));
        event.setReminders(reminders);

        // create event
        event = service.events().insert(calendarId, event).execute();

        //System.out.printf("Event created: %s\n", event.getHtmlLink());
    }

    /**
     * Gets a list of users calendars
     *
     * @return ObservableList<calendarChoice>
     * @throws IOException
     */
    static ObservableList<calendarChoice>
    calendars(com.google.api.services.calendar.Calendar service) throws IOException {
        ObservableList<calendarChoice> cList = FXCollections.observableArrayList();
        calendarChoice choice;
        String pageToken = null;
        do {
            CalendarList calendarList = service.calendarList().list().setPageToken(pageToken).execute();
            List<CalendarListEntry> items = calendarList.getItems();

            for (CalendarListEntry calendarListEntry : items) {
                choice = new calendarChoice();
                choice.key = calendarListEntry.getId();
                choice.value = calendarListEntry.getSummary();
                cList.add(choice);
            }
            pageToken = calendarList.getNextPageToken();
        } while (pageToken != null);

        return cList;
    }

    void setCalendar(String cId) {
        calendarId = cId;
    }
}

/**
 * simple google calendar holderclass
 * */
class calendarChoice {
    String key;
    String value;
    /**
     * human readable string for javaFX select
     * */
    @Override public String toString() { return value; }
}
