package main;

/**
 * Created by erik.schults on 07.12.2016.
 */

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Logging into OIS and getting the schedu;e
 */
public class OisClient {
    private static String studentId;
    private static String responseBody;
    private static List<Lecture> schedule;
    private static BasicCookieStore cookieStore = new BasicCookieStore();
    private static CloseableHttpResponse response;
    private static CloseableHttpClient httpclient = HttpClients.custom()
            .setDefaultCookieStore(cookieStore)
            .build();
    /**
     * HttpResponse into string
     * */
    private static ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
        @Override
        public String handleResponse(
                final HttpResponse response) throws ClientProtocolException, IOException {
            HttpEntity entity = response.getEntity();
            return EntityUtils.toString(entity);
        }

    };

    private static void login(String user, String pw) throws IOException, URISyntaxException {
        HttpUriRequest login = RequestBuilder.post()
                .setUri(new URI("https://itcollege.ois.ee/auth/login"))
                .addParameter("continue", "/")
                .addParameter("username", user)
                .addParameter("pw", pw)
                .addParameter("Login", "Logi+sisse")
                .build();
        response = httpclient.execute(login);
        response.close();
    }

    private static void getStudentId() throws IOException {
        String url = "https://itcollege.ois.ee/";
        Pattern p = Pattern.compile("(?<=\\?student_id=)(\\d+)");
        Matcher m;
        HttpGet httpget = new HttpGet(url);
        responseBody = httpclient.execute(httpget, responseHandler);
        m = p.matcher(responseBody);
        if(m.find()) studentId = m.group(1);
        response.close();
    }

    /**
     * @return current weeks monday string
     * */
    private static String getMonday() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(c.getTime());
    }

    private static void schedule() throws IOException, ParseException {
        String calendarUrl =
                "https://itcollege.ois.ee/et/timetable?Submit&student_id=:student_id&period=month&view=month&start_date=:monday";
        Pattern p = Pattern.compile("(^[\\d]*:[\\d]*) - ([\\d]*:[\\d]*) (.+?) ([\\d+, ]+) ([\\d+, ]+) ([\\p{L} ]*)");
        Pattern groups = Pattern.compile("Rühmad:(.*?)<");
        Pattern time = Pattern.compile("Aeg: (\\d*:\\d*) - (\\d*:\\d*)<");
        Pattern location = Pattern.compile("Ruum: (.*?)<");
        Pattern teacher = Pattern.compile("Õppejõud: (.*?)<");
        Pattern type = Pattern.compile("Tüüp: (.*?)<");
        Pattern name = Pattern.compile("Voor: (\\D+)");
        schedule = new ArrayList<>();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date now = new Date();
        HttpGet httpget = new HttpGet(calendarUrl.replace(":student_id", studentId).replace(":monday", getMonday()));
        responseBody = httpclient.execute(httpget, responseHandler);
        Document doc = Jsoup.parse(responseBody);
        Elements dayParents = doc.select(".inner-day");
        for (Element dayParent : dayParents) {
            String date = dayParent.parent().attr("id");
            // skip old dates
            if (!df.parse(date).after(now) && !df.parse(date).equals(df.parse(df.format(now)))) continue;

            Elements lectures = dayParent.select(".monthEvent");
            for (Element lecture : lectures) {
                String html = lecture.outerHtml();
                if (html.trim().length() == 0) continue;

                Matcher m;
                Lecture tempLecture = new Lecture();
                tempLecture.date = date;

                m = time.matcher(html);
                if (m.find()) {
                    tempLecture.startTime = m.group(1);
                    tempLecture.endTime = m.group(2);
                }

                m = name.matcher(html);
                if (m.find()) {
                    tempLecture.name = m.group(1).trim();
                }

                m = groups.matcher(html);
                if (m.find()) {
                    tempLecture.groups = m.group(1).trim();
                }

                m = location.matcher(html);
                if (m.find()) {
                    tempLecture.location = m.group(1).trim();
                }

                m = type.matcher(html);
                if (m.find()) {
                    tempLecture.type = m.group(1).trim();
                }

                m = teacher.matcher(html);
                if (m.find()) {
                    tempLecture.teacher = m.group(1).trim();
                }
                schedule.add(tempLecture);
            }
        }
    }

    public static List<Lecture> getSchedule(String user, String pw) throws IOException, ParseException, URISyntaxException {
        System.setProperty("jsse.enableSNIExtension", "false"); // connection to https fails otherwise
        login(user, pw);
        getStudentId();
        schedule();
        httpclient.close();
        return schedule;
    }
}

/**
 * A simple class which is made to hold lecture values from the ÕIS schedule
 * Created by erik.schults on 01.11.2016.
 */
class Lecture {
    String date;
    String startTime;
    String endTime;
    String name;
    String groups;
    String location;
    String teacher;
    String type;
    boolean selected = true;

    /**
     * returns the start datetime of a concurrent lecture in a suitable format for google api
     * */
    String getStartDateTime() {
        return date + "T" + startTime +":00Z";
    }

    /**
     * returns the end datetime of a concurrent lecture in a suitable format for google api
     * */
    String getEndDateTime() {
        return date + "T" + endTime +":00Z";
    }

    /**
     * to be used for javaFX class to toggle lecture active/inactive on click
     * */
    void select() {
        this.selected = !this.selected;
    }

    @Override public String toString() { return date + " " + startTime + " " + endTime + " " +name + " " +groups + " " + location + " " + teacher + " " + type; }

}
