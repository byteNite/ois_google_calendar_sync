package main;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static javafx.application.Platform.exit;
import static main.OisClient.getSchedule;

public class Main extends Application {
    private static GoogleSync gs = new GoogleSync();
    private static com.google.api.services.calendar.Calendar service;
    private static List<Lecture> schedule;
    private static Stage stage;
    private static Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    private static Scene scene;
    private static StackPane root;
    private static ObservableList<calendarChoice> calendars;
    @Override
    public void start(Stage primaryStage) throws Exception{
        stage = primaryStage;
        service = GoogleSync.getCalendarService();

        GridPane loginForm = loginForm(service);

        setScene(loginForm);
        stage.setTitle("Doing some calendrish shit!");
        stage.show();
    }

    private static void setScene(Parent obj) {
        root = new StackPane();
        root.setPadding(new Insets(50));
        root.getChildren().add(obj);
        stage.setScene(new Scene(root));
        stage.show();
        stage.setX(d.width/2-(stage.getWidth()/2));
        stage.setY(d.height/2-(stage.getHeight()/2));
    }

    private static GridPane loginForm(com.google.api.services.calendar.Calendar service) {
        GridPane grid = new GridPane();
        grid.setVgap(10);
        grid.setAlignment(Pos.CENTER);

        Text Ois = new Text("õis login");
        Ois.setId("header");
        grid.add(Ois, 0, 0, 2, 1);

        Label usrLbl = new Label("Username");
        TextField usrField = new TextField();
        grid.add(usrLbl, 0, 1);
        grid.add(usrField, 1, 1);

        Label pwLbl = new Label("Password");
        PasswordField pwField = new PasswordField();
        grid.add(pwLbl, 0, 2);
        grid.add(pwField, 1, 2);

        Button submit = new Button("Login");

        HBox box = new HBox();
        Label error = new Label("");
        error.setTextFill(Paint.valueOf("#f00"));
        box.setAlignment(Pos.BOTTOM_RIGHT);
        box.getChildren().add(submit);
        grid.add(error, 0, 5, 2, 1);
        grid.add(box, 1, 4);
        submit.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                try {
                    schedule = getSchedule(usrField.getText(), pwField.getText());
                } catch (Throwable e) {
                    error.setText("Bad Credentials");
                }
                try {
                    calendars = GoogleSync.calendars(service);
                    calendar();
                } catch (IOException e) {
                    error.setText("Error connecting with google");
                }
            }
        });

        return grid;
    }


    private static void calendarSelect() {
        VBox box = new VBox(5);
        box.setAlignment(Pos.CENTER);

        ChoiceBox<calendarChoice> ch = new ChoiceBox<>(calendars);
        ch.getSelectionModel().select(0);
        Text heading = new Text("Select a calendar");
        heading.setFont(Font.font("Verdana", FontWeight.NORMAL, 14));

        Button next = new Button("Next");
        Button back = new Button("Back");
        BorderPane buttons = new BorderPane();
        buttons.setLeft(back);
        buttons.setRight(next);

        ch.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<calendarChoice>() {
            @Override
            public void changed(ObservableValue<? extends calendarChoice> observable, calendarChoice oldValue, calendarChoice newValue) {
                gs.setCalendar(newValue.key);
            }
        });
        next.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                setScene(confirm(calendars));
            }
        });
        back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                calendar();
            }
        });

        box.getChildren().addAll(heading, ch, buttons);
        setScene(box);
    }

    private static VBox confirm(ObservableList<calendarChoice> calendars) {
        VBox box = new VBox(5);
        HBox buttons = new HBox(10);
        buttons.setAlignment(Pos.BASELINE_CENTER);
        box.setAlignment(Pos.CENTER);
        box.setSpacing(20);
        Text confrimationMessage = new Text("Are you sure you want to do this?");
        confrimationMessage.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        confrimationMessage.setFill(Paint.valueOf("#f00"));
        Button confirm = new Button("Yes");
        Button back = new Button("No");
        back.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                calendarSelect();
            }
        });
        /*
        * Adding events loop
        * */
        confirm.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                for(Lecture lecture: schedule) {
                    if (!lecture.selected) continue;
                    try {
                        gs.addEvent(service, lecture);
                    } catch (Exception e) {
                        System.out.println("tra na");
                    }
                }
                exit();
            }
        });

        buttons.getChildren().addAll(back, confirm);
        box.getChildren().addAll(confrimationMessage, buttons);
        return box;
    }

    private static void editLecture(int I, Lecture l) {
        GridPane g = new GridPane();
        g.setPadding(new Insets(10, 5, 5, 5));
        Label namelbl = new Label("Name:");
        TextField name = new TextField();
        name.setText(l.name);
        Label typelbl = new Label("Type:");
        TextField type = new TextField();
        type.setText(l.type);
        Label teacherlbl = new Label("Teacher:");
        TextField teacher = new TextField();
        teacher.setText(l.teacher);

        BorderPane buttons = new BorderPane();
        Button close = new Button("Close");
        Button del = new Button("Delete");
        Button save = new Button("Save");
        close.setOnAction(event1 -> calendar());
        del.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                schedule.remove(I); calendar();
            }
        });
        save.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent event) {
                schedule.get(I).name = name.getText();
                schedule.get(I).type = type.getText();
                schedule.get(I).teacher = teacher.getText();
                calendar();
            }
        });
        g.add(namelbl, 0, 0);
        g.add(name, 1, 0, 3, 1);
        g.add(typelbl, 0, 1);
        g.add(type, 1, 1, 3, 1);
        g.add(teacherlbl, 0, 2);
        g.add(teacher, 1, 2, 3, 1);
        g.add(close, 0, 4);
        g.add(del, 1, 4);
        g.add(save, 2, 4);
        setScene(g);
    }
    private static int getWeekDay(String date) {
        //System.out.println(getWeekDay("2016-11-11T12:45");
        LocalDate fullDate = LocalDate.parse(date, DateTimeFormatter.ISO_DATE);
        return fullDate.getDayOfWeek().getValue();
    }

    private static String getDate(String date) {
        LocalDateTime fullDate = LocalDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME);
        return fullDate.toLocalDate().toString();
    }

    private static void calendar() {
        GridPane grid = new GridPane();
        grid.setHgap(15);
        grid.add(new Text("Esmaspäev"), 0, 0);
        grid.add(new Text("Teisipäev"), 1, 0);
        grid.add(new Text("Kolmapäev"), 2, 0);
        grid.add(new Text("Neljapäev"), 3, 0);
        grid.add(new Text("Reede"), 4, 0);
        int row = 1;
        int col;
        String lastDate = "";
        VBox container = new VBox(2);
        container.setPadding(new Insets(5));
        for (int i = 0; i < schedule.size(); i++) {
            Lecture lecture = schedule.get(i);
            col = getWeekDay(lecture.date) - 1;

            if (!lecture.date.equals(lastDate)) {
                Text date = new Text(lecture.date);
                date.setFont(Font.font("Verdana", FontWeight.BOLD, 13));
                container.getChildren().addAll(date, new Separator());
            }

            final int I = i;
            Text desc = new Text();
            if (lecture.name == null) {
                desc.setText(lecture.teacher + " " + lecture.type + " " + lecture.startTime);
            } else {
                desc.setText(lecture.name + " " + lecture.type + " " + lecture.startTime);
            }
            desc.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    if (event.getButton() == MouseButton.SECONDARY) {
                        editLecture(I, lecture);
                    }
                    else {
                        if(!schedule.get(I).selected) {
                            desc.setStyle("-fx-fill: dodgerblue");
                        } else {
                            desc.setStyle("-fx-fill: red");
                        }
                        schedule.get(I).select();
                    }
                }
            });

            container.getChildren().add(desc);

            try {
                if (!lecture.date.equals(schedule.get(i + 1).date)) {
                    grid.add(container, col, row);
                    container = new VBox(2);
                    container.setPadding(new Insets(5));
                }
            } catch (Exception e) {
                grid.add(container, col, row);
            }

            try {
                if (col > getWeekDay(schedule.get(i + 1).date)) {
                    row++;
                }
            } catch (IndexOutOfBoundsException e) {
                continue;
            }
            lastDate = lecture.date;
        }
        Button next = new Button("Next");
        next.setOnAction(event -> calendarSelect());

        HBox buttonFloat = new HBox(5);
        buttonFloat.setAlignment(Pos.BOTTOM_RIGHT);
        buttonFloat.getChildren().add(next);
        grid.add(buttonFloat, 0, row+2, 7, 1);
        setScene(grid);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
